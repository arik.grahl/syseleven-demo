# SysEleven Demo

## Building

```
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --use
docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 --pull --no-cache -t registry.gitlab.com/arik.grahl/syseleven-demo:v0.0.3 -t registry.gitlab.com/arik.grahl/syseleven-demo:latest --push .
docker buildx rm
```

## Deploying

```
cd helm
helm install live .
```

## Updating

```
cd helm
helm upgrade live .
```

## Metrics

```
kubectl -n syseleven-kube-prometheus-stack get secrets kube-prometheus-stack-grafana -o json | jq '.data."admin-password"' -r | base64 -d | xclip -selection clipboard -i
kubectl -n syseleven-kube-prometheus-stack port-forward svc/kube-prometheus-stack-grafana 3000:80
```

```
gdb-open http://localhost:3000/d/MsjffzSZz/nginx?orgId=1&refresh=5s&from=now-3h&to=now
```

## Logs

```
kubectl logs -f -l app.kubernetes.io/instance=live -l app.kubernetes.io/name=syseleven-demo -c syseleven-demo
```

## Testing

```
cd terratest
go test -v -tags helm -run TestIntegration
```
