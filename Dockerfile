FROM ruby:3.1.2 AS builder

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN bundle install --path vendor
RUN bundle exec jekyll build -d public

FROM nginx:1.23.1-alpine

# mitigate CVE-2022-2309 and CVE-2022-37434

RUN \
	apk update && \
	apk upgrade && \
	rm -rfv /var/cache/apk/*

COPY nginx/stub_status.conf /etc/nginx/conf.d/stub.conf

COPY --from=builder /usr/src/app/public /usr/share/nginx/html
